<?php


class Cart
{
    private $salesOrder;
    private $simples;

    const CART_COOKIE_EXPIRATION = '+30 days';
    const CART_SESSION_SALES_ORDER = 'cartSalesOrder';
    const CART_SESSION_SIMPLES = 'cartSimples';

    private $maxBuyPrice = 4999.99;
    private $maxBuyItem = 15;

    /**
     * Loads and initializes the cart
     */
    public function __construct ()
    {
        $this->salesOrder = Session::get(self::CART_SESSION_SALES_ORDER);
        $this->simples = Session::get(self::CART_SESSION_SIMPLES);

        if (empty($this->simples)) {
            $this->simples = array();
            Session::add(self::CART_SESSION_SIMPLES, $this->simples);
        }

    }



    /**
     * Returns all current items
     *
     * @return array
     */
    public function getItems ()
    {
        return $this->salesOrder->getItemCollection()->getItems();
    }



    public function __destruct ()
    {
        Session::add(self::CART_SESSION_SALES_ORDER, $this->salesOrder);
    }
}
