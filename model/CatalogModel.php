<?php

require_once __DIR__."/../adapter/SolrAdapter.php";

class CatalogModel
{
    /**
     * @var SolrAdapter
     */
    private $solrAdapter;

    /**
     * @param string $search
     * @return array<Product>
     */
    public function getResultSearch($search)
    {
        $url = $this->getSolrAdapter()->getSearchUrl($search);
        $result = $this->getSolrAdapter()->call($url);
        return $this->convertSolrResult($result);
    }


    /**
     * @return SolrAdapter
     */
    protected function getSolrAdapter()
    {
        if (is_null($this->solrAdapter)) {
            $this->solrAdapter = new SolrAdapter();
        }
        return $this->solrAdapter;
    }

    /**
     * @return array<Product>
     */
    protected function convertSolrResult($result)
    {
        return $result;
    }

    public function setSolrAdapter(SolrAdapter $solrAdapter)
    {
        $this->solrAdapter = $solrAdapter;
    }
}
