<?php

/**
 * Cenário 1: Testar o método getProductBySku($sku)
 *
 *
 *
 */


class ProductModel
{

    /**
     * @var MemcacheAdapter
     */
    protected $memcacheAdapter;

    /**
     * @param MemcacheAdapter $memcacheAdapter
     */
    public function __construct(MemcacheAdapter $memcacheAdapter)
    {
        $this->memcacheAdapter = $memcacheAdapter;
    }


    /**
     * @param $sku
     * @return Product
     */
    public function getProductBySku($sku)
    {
        $memcacheKey = $this->memcacheAdapter->generatekeyMemcacheForProduct($sku);
        return $this->memcacheAdapter->get($memcacheKey);
    }
}
