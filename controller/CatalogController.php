<?php

require_once __DIR__.'/Controller.php';
require_once __DIR__.'/../model/ProductModel.php';



/**
 *  Cenário 2: testar método actionSearch()
 */


class CatalogController extends Controller
{

    /**
     * @var CatalogModel
     */
    private $catalogModel;

    private $view;

    /**
     * @var array
     */
    private $products;


    public function actionSearch()
    {
        $search = $this->getParam('q');
        $this->products = $this->getCatalogModel()->getResultSearch($search);

        $this->renderView('catalog', $this->products);
    }

    /**
     * @return CatalogModel
     */
    protected function getCatalogModel()
    {
        if (is_null($this->catalogModel)) {
            $this->catalogModel = new CatalogModel();
        }
        return $this->catalogModel;
    }
}
