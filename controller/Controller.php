<?php

require_once __DIR__.'/../adapter/SolrAdapter.php';
require_once __DIR__.'/../adapter/MemcacheAdapter.php';
require_once __DIR__.'/../component/Session.php';
require_once __DIR__.'/../model/Cart.php';

abstract class Controller
{
    /**
     * @var SolrAdapter
     */
    protected  $solrAdapter;


    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var array
     */
    protected $messages;


    protected function __construct()
    {
        $this->init();
    }



    /**
     * @return string
     */
    protected function getParam()
    {
        // get GET/POST parameter
    }

    /**
     * @param $nameView
     */
    protected function renderView($nameView, $data)
    {
        //load view
    }
}
