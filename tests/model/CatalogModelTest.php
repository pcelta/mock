<?php
require_once __DIR__ ."/../../model/CatalogModel.php";
require_once __DIR__ ."/../../adapter/SolrAdapter.php";

class CatalogModelTest extends PHPUnit_Framework_TestCase
{

    public function testGetResultSearchShouldReturnResultWhenExistsResult()
    {
        $catalogModel = new CatalogModel();

        $mockedSolrAdapter = $this->getMockBuilder("SolrAdapter")
            ->setMethods(array("call"))
            ->disableOriginalConstructor()
            ->getMock();

        $search = "calcados";
        $solrUrl = "http://localhost:8081?q=". $search;

        $mockedSolrAdapter->expects($this->once())
            ->method('call')
            ->with($solrUrl)
            ->will($this->returnValue(array(1)));

        $catalogModel->setSolrAdapter($mockedSolrAdapter);
        $result = $catalogModel->getResultSearch("calcados");

        $this->assertNotEmpty($result);
    }
} 